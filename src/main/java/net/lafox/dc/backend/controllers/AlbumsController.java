package net.lafox.dc.backend.controllers;

import net.lafox.dc.backend.entity.Album;
import net.lafox.dc.backend.entity.ErrorReport;
import net.lafox.dc.backend.service.AlbumsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/albums", produces = "application/json")
@CrossOrigin
public class AlbumsController {
    final AlbumsService albumsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AlbumsController.class);

    @Autowired
    public AlbumsController(AlbumsService albumsService) {
        this.albumsService = albumsService;
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleEntityException(Exception e) {
        LOGGER.warn("API Exception: ", e);
        return new ResponseEntity<>(ErrorReport.of(e.getCause().getMessage(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    ResponseEntity<List<Album>> findAll() {
        return new ResponseEntity<>(albumsService.findAll(), HttpStatus.OK);
    }

    @PatchMapping(consumes = "application/json")
    ResponseEntity<Album> patch(@RequestBody Album newAlbum) {
        return new ResponseEntity<>(albumsService.patch(newAlbum), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@PathVariable Long id) {
        albumsService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
