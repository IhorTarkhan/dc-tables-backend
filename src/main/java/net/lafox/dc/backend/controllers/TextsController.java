package net.lafox.dc.backend.controllers;

import net.lafox.dc.backend.entity.ErrorReport;
import net.lafox.dc.backend.entity.Text;
import net.lafox.dc.backend.service.TextsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/texts", produces = "application/json")
@CrossOrigin
public class TextsController {
    final TextsService textsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AlbumsController.class);

    @Autowired
    public TextsController(TextsService textsService) {
        this.textsService = textsService;
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleEntityException(Exception e) {
        LOGGER.warn("API Exception: ", e);
        return new ResponseEntity<>(ErrorReport.of(e.getCause().getMessage(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    ResponseEntity<List<Text>> findAll(@RequestParam(name = "songId", defaultValue = "0", required = false) Long songId) {
        return new ResponseEntity<>(textsService.findAll(songId), HttpStatus.OK);
    }

    @PatchMapping(consumes = "application/json")
    ResponseEntity<Text> patch(@RequestBody Text newText) {
        return new ResponseEntity<>(textsService.patch(newText), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@PathVariable Long id) {
        textsService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
