package net.lafox.dc.backend.controllers;

import net.lafox.dc.backend.entity.ErrorReport;
import net.lafox.dc.backend.entity.Language;
import net.lafox.dc.backend.entity.PathLanguageDTO;
import net.lafox.dc.backend.service.LanguagesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/languages", produces = "application/json")
@CrossOrigin
public class LanguagesController {
    final LanguagesService languagesService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AlbumsController.class);

    @Autowired
    public LanguagesController(LanguagesService languagesService) {
        this.languagesService = languagesService;
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleEntityException(Exception e) {
        LOGGER.warn("API Exception: ", e);
        return new ResponseEntity<>(ErrorReport.of(e.getCause().getMessage(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    ResponseEntity<List<Language>> findAll() {
        return new ResponseEntity<>(languagesService.findAll(), HttpStatus.OK);
    }

    @PatchMapping(consumes = "application/json")
    ResponseEntity<Language> patch(@RequestBody PathLanguageDTO pathLanguageDTO) {
        return new ResponseEntity<>(languagesService.patch(pathLanguageDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{name}")
    ResponseEntity<Object> delete(@PathVariable String name) {
        languagesService.delete(name);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
