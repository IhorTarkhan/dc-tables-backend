package net.lafox.dc.backend.controllers;

import net.lafox.dc.backend.entity.ErrorReport;
import net.lafox.dc.backend.entity.Song;
import net.lafox.dc.backend.entity.SongBriefInfo;
import net.lafox.dc.backend.service.SongsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/songs", produces = "application/json")
@CrossOrigin
public class SongsController {
    final SongsService songsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AlbumsController.class);

    @Autowired
    public SongsController(SongsService songsService) {
        this.songsService = songsService;
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleEntityException(Exception e) {
        LOGGER.warn("API Exception: ", e);
        return new ResponseEntity<>(ErrorReport.of(e.getCause().getMessage(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    ResponseEntity<List<Song>> findAllInAlbum(@RequestParam(name = "albumId", defaultValue = "0", required = false) Long albumId) {
        return new ResponseEntity<>(songsService.findAll(albumId), HttpStatus.OK);
    }

    @PatchMapping( consumes = "application/json")
    ResponseEntity<Song> patch(@RequestBody Song newSong) {
        return new ResponseEntity<>(songsService.patch(newSong), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@PathVariable Long id) {
        songsService.delete(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping("/briefInfo")
    ResponseEntity<List<SongBriefInfo>> fetchAllSongBriefInfo() {
        return new ResponseEntity<>(songsService.fetchAllSongBriefInfo(), HttpStatus.OK);
    }
}
