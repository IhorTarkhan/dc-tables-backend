package net.lafox.dc.backend.entity;

import lombok.Data;

@Data
public class PathLanguageDTO {
    String oldName;
    String newName;
}
