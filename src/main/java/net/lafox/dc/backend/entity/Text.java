package net.lafox.dc.backend.entity;

import lombok.Data;

@Data
public class Text {
    Long id;
    String text;
    String language;
    Long songId;
}
