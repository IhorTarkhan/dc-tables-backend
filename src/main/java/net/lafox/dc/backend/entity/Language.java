package net.lafox.dc.backend.entity;

import lombok.Data;

@Data
public class Language {
    String name;
}
