package net.lafox.dc.backend.entity;

import lombok.Data;

@Data
public class Song {
    Long id;
    String name;
    Long albumId;
    Integer orderInAlbum;
}
