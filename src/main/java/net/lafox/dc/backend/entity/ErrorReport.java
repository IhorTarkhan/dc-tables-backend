package net.lafox.dc.backend.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@RequiredArgsConstructor(staticName = "of")
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class ErrorReport {
    final String message;
    final String cause;

    public static ErrorReport of(Exception e) {
        return ErrorReport.of(e.getMessage(), e.toString());
    }
}
