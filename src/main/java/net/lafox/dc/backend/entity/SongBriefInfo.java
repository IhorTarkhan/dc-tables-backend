package net.lafox.dc.backend.entity;

import lombok.Data;

@Data
public class SongBriefInfo {
    Long id;
    String songName;
    String albumName;
}
