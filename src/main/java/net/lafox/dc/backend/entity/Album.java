package net.lafox.dc.backend.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Album {
    Long id;
    String name;
    String imageId;
    LocalDate releaseDate;
}
