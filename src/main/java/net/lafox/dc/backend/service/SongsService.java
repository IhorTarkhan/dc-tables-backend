package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Song;
import net.lafox.dc.backend.entity.SongBriefInfo;

import java.util.List;

public interface SongsService {
    List<Song> findAll(Long albumId);

    Song patch(Song newSong);

    void delete(Long id);

    List<SongBriefInfo> fetchAllSongBriefInfo();
}
