package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Album;

import java.util.List;

public interface AlbumsService {
    List<Album> findAll();

    Album patch(Album newAlbum);

    void delete(Long id);
}
