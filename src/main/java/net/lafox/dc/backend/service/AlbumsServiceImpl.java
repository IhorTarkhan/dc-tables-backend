package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Album;
import net.lafox.dc.backend.mapper.AlbumsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AlbumsServiceImpl implements AlbumsService {
    private final AlbumsMapper albumsMapper;

    @Autowired
    public AlbumsServiceImpl(AlbumsMapper albumsMapper) {
        this.albumsMapper = albumsMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Album> findAll() {
        return albumsMapper.findAll();
    }

    @Override
    public Album patch(Album newAlbum) {
        if (newAlbum.getId() == null || newAlbum.getId() == 0) {
            albumsMapper.insert(newAlbum);
        } else {
            int numberOfChanges = albumsMapper.update(newAlbum);
            if (numberOfChanges == 0) {
                throw new RuntimeException("Album with id=" + newAlbum.getId() + " do not exist");
            }
        }
        return newAlbum;
    }

    @Override
    public void delete(Long id) {
        int numberOfChanges = albumsMapper.delete(id);
        if (numberOfChanges == 0) {
            throw new RuntimeException("Album with id=" + id + " do not exist");
        }
    }
}
