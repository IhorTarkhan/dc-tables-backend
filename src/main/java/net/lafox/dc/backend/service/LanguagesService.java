package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Language;
import net.lafox.dc.backend.entity.PathLanguageDTO;

import java.util.List;

public interface LanguagesService {
    List<Language> findAll();

    Language patch(PathLanguageDTO pathLanguageDTO);

    void delete(String name);
}
