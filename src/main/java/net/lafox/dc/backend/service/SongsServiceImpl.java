package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Song;
import net.lafox.dc.backend.entity.SongBriefInfo;
import net.lafox.dc.backend.mapper.SongsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SongsServiceImpl implements SongsService {
    private final SongsMapper songsMapper;

    @Autowired
    public SongsServiceImpl(SongsMapper songsMapper) {
        this.songsMapper = songsMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Song> findAll(Long albumId) {
        if (albumId == null || albumId == 0) {
            return songsMapper.findAll();
        } else {
            return songsMapper.findAllInAlbum(albumId);
        }
    }

    @Override
    public Song patch(Song newSong) {
        if (newSong.getId() == null || newSong.getId() == 0) {
            songsMapper.insert(newSong);
        } else {
            songsMapper.update(newSong);
        }
        return newSong;
    }

    @Override
    public void delete(Long id) {
        songsMapper.delete(id);
    }

    @Override
    public List<SongBriefInfo> fetchAllSongBriefInfo() {
        return songsMapper.fetchAllSongBriefInfo();
    }
}
