package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Language;
import net.lafox.dc.backend.entity.PathLanguageDTO;
import net.lafox.dc.backend.mapper.LanguageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.util.StringUtils.isEmpty;

@Service
@Transactional
public class LanguagesServiceImpl implements LanguagesService {
    private final LanguageMapper languageMapper;

    @Autowired
    public LanguagesServiceImpl(LanguageMapper languageMapper) {
        this.languageMapper = languageMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Language> findAll() {
        return languageMapper.findAll();
    }

    @Override
    public Language patch(PathLanguageDTO pathLanguageDTO) {
        if (isEmpty(pathLanguageDTO.getOldName())) {
            languageMapper.insert(pathLanguageDTO.getNewName());
        } else {
            languageMapper.update(pathLanguageDTO.getOldName(), pathLanguageDTO.getNewName());
        }
        Language newLanguage = new Language();
        newLanguage.setName(pathLanguageDTO.getNewName());
        return newLanguage;
    }

    @Override
    public void delete(String name) {
        languageMapper.delete(name);
    }
}
