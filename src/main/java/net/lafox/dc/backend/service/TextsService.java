package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Text;

import java.util.List;

public interface TextsService {
    List<Text> findAll(Long songId);

    Text patch(Text newText);

    void delete(Long id);
}
