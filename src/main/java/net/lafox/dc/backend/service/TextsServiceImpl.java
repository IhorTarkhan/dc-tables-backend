package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Text;
import net.lafox.dc.backend.mapper.TextsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TextsServiceImpl implements TextsService {
    private final TextsMapper textsMapper;

    @Autowired
    public TextsServiceImpl(TextsMapper textsMapper) {
        this.textsMapper = textsMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Text> findAll(Long songId) {
        if (songId == null || songId == 0) {
            return textsMapper.findAll();
        } else {
            return textsMapper.findAllInSong(songId);
        }
    }

    @Override
    public Text patch(Text newText) {
        if (newText.getId() == null) {
            textsMapper.insert(newText);
        } else {
            textsMapper.update(newText);
        }
        return newText;
    }

    @Override
    public void delete(Long id) {
        textsMapper.delete(id);
    }
}
