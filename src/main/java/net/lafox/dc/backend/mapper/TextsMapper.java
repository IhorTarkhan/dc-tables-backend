package net.lafox.dc.backend.mapper;

import net.lafox.dc.backend.entity.Text;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface TextsMapper {
    @Select("SELECT t.* FROM texts t " +
            "       LEFT JOIN songs s ON t.song_id = s.id " +
            "       LEFT JOIN albums a ON s.album_id = a.id " +
            "ORDER BY a.release_date, s.order_in_album, t.language;")
    List<Text> findAll();

    @Select("SELECT t.* FROM texts t " +
            "WHERE t.song_id = #{songId} " +
            "ORDER BY t.language;")
    List<Text> findAllInSong(@Param("songId") Long songId);

    @Update("UPDATE texts SET text = #{text}, language = #{language}, song_id = #{songId} WHERE id = #{id};")
    void update(Text text);

    @Insert("INSERT INTO texts (id, text, language, song_id) VALUES (DEFAULT, #{text}, #{language}, #{songId});")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Text text);

    @Delete("DELETE FROM texts WHERE id = #{id};")
    void delete(@Param("id") Long id);
}
