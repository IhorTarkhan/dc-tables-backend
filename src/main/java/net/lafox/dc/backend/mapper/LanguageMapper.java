package net.lafox.dc.backend.mapper;

import net.lafox.dc.backend.entity.Language;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface LanguageMapper {
    @Select("SELECT * FROM languages ORDER BY name;")
    List<Language> findAll();

    @Update("UPDATE languages SET name= #{new_name} WHERE name = #{old_name};")
    void update(@Param("old_name") String oldName, @Param("new_name") String newName);

    @Insert("INSERT INTO languages (name) VALUES (#{new_name});")
    void insert(@Param("new_name") String newName);

    @Delete("DELETE FROM languages WHERE name = #{name};")
    void delete(@Param("name") String name);
}
