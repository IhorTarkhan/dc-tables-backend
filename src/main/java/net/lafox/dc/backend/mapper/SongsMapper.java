package net.lafox.dc.backend.mapper;

import net.lafox.dc.backend.entity.Song;
import net.lafox.dc.backend.entity.SongBriefInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface SongsMapper {
    @Select("SELECT s.* FROM songs s LEFT JOIN albums a ON a.id = s.album_id ORDER BY a.release_date, s.order_in_album;")
    List<Song> findAll();

    @Select("SELECT s.*, a.name AS album_name FROM songs s LEFT JOIN albums a ON a.id = s.album_id WHERE album_id = #{albumId} ORDER BY order_in_album;")
    List<Song> findAllInAlbum(@Param("albumId") Long albumId);

    @Update("UPDATE songs SET name = #{name}, album_id = #{albumId}, order_in_album = #{orderInAlbum} WHERE id = #{id};")
    void update(Song song);

    @Insert("INSERT INTO songs (id, name, album_id, order_in_album) VALUES (DEFAULT, #{name}, #{albumId}, #{orderInAlbum});")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Song song);

    @Delete("DELETE FROM songs WHERE id = #{id};")
    void delete(@Param("id") Long id);

    @Select("SELECT s.id, s.name AS song_name, a.name AS album_name FROM songs s LEFT JOIN albums a on s.album_id = a.id ORDER BY a.release_date, s.order_in_album;")
    List<SongBriefInfo> fetchAllSongBriefInfo();
}
