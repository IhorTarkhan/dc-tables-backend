package net.lafox.dc.backend.mapper;

import net.lafox.dc.backend.entity.Album;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface AlbumsMapper {
    @Select("SELECT * FROM albums ORDER BY release_date;")
    List<Album> findAll();

    @Update("UPDATE albums SET name = #{name}, image_id = #{imageId}, release_date = #{releaseDate} WHERE id = #{id};")
    int update(Album album);

    @Insert("INSERT INTO albums (id, name, image_id, release_date) VALUES (DEFAULT, #{name}, #{imageId}, #{releaseDate});")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Album album);

    @Delete("DELETE FROM albums WHERE id = #{id};")
    int delete(@Param("id") Long id);
}
