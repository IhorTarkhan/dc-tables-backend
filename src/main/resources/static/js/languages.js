const Languages = {
    composeLanguage: function (language) {
        return '<tr id="' + language.name + '">' +
            '       <td><input class="name" value="' + ((language.name[0] === '_') ? "" : language.name) + '"/></td>' +
            '       <td><button class="saveButton">Save</button></td>' +
            '       <td><button class="deleteButton">Delete</button></td>' +
            '   </tr>';
    },
    loadLanguages: function (tableObject) {
        $.getJSON(languageAPI.getAllLanguages(),
            function (languageList) {
                for (let i = 0; i < languageList.length; i++) {
                    $(tableObject).append(Languages.composeLanguage(languageList[i]));
                }
            }
        );
        Languages.onClickSaveLanguageButton(tableObject);
        Languages.onClickDeleteLanguageButton(tableObject);
    },
    onClickSaveLanguageButton: function (tableObject) {
        $(tableObject).on('click', '.saveButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            const newName = $(rowObject).find(".name").val();
            let oldName = $(rowObject)[0].id;
            if (oldName[0] === '_') {
                oldName = "";
            }
            $.patchJSON(languageAPI.patch(), {
                "oldName": oldName,
                "newName": newName
                },
                function (result) {
                    $(rowObject).attr("id", result.name);
                    alert("Saved");
                },
                function (result) {
                    alertError(result);
                }
            );
        });
    },
    onClickDeleteLanguageButton: function (tableObject) {
        $(tableObject).on('click', '.deleteButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            if (!confirm('Delete language?')) {
                return;
            }
            if ($(rowObject)[0].id[0] === "_") {
                $(rowObject).remove();
            } else {
                $.deleteJSON(languageAPI.delete($(rowObject)[0].id),
                    function () {
                        $(rowObject).remove();
                    },
                    function (result) {
                        alertError(result);
                    }
                );
            }
        });
    },
    languageCreatingOrder: 0,
    onClickCreateLanguage: function (createNewSongButton, tableObject) {
        $(createNewSongButton).on('click', function () {
            const inCreatingOrder = ++Languages.languageCreatingOrder;
            $(tableObject).append(Languages.composeLanguage({
                name: '_' + inCreatingOrder
            }));
        });
    }
}