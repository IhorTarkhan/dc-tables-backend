const Texts = {
    composeTextHtmlRow: function (text) {
        return '<tr id="' + text.id + '">' +
            '       <td><p class="id">' + ((text.id[0] === '_') ? "" : text.id) + '</p></td>' +
            '       <td><select class="song"><option value="-1" selected disabled hidden>Select Song</option></select></td>' +
            '       <td><select class="language"><option value="-1" selected disabled hidden>Select Language</option></select></td>' +
            '       <td><textarea class="text">' + text.text + '</textarea></td>' +
            '       <td><button class="saveButton">Save</button></td>' +
            '       <td><button class="deleteButton">Delete</button></td>' +
            '   </tr>';
    },
    fillHtmlSongList: function (textList) {
        $.getJSON(songAPI.fetchAllSongSortInfo(),
            function (shortSongsInfoList) {
                for (let i = 0; i < shortSongsInfoList.length; i++) {
                    const song = shortSongsInfoList[i];
                    const option = '<option value="' + song.id + '">' + song['albumName'] + ': ' + song['songName'] + '</option>';
                    $('.song').append(option);
                }
                for (let i = 0; i < textList.length; i++) {
                    const text = textList[i];
                    $('#' + text.id + ' .song').val(text['songId']);
                }
            }
        );
    },
    fillHtmlLanguageList: function (textList) {
        $.getJSON(languageAPI.getAllLanguages(),
            function (languagesList) {
                for (let i = 0; i < languagesList.length; i++) {
                    const language = languagesList[i];
                    const option = '<option value="' + language.name + '">' + language.name + '</option>';
                    $('.language').append(option);
                }
                for (let i = 0; i < textList.length; i++) {
                    const text = textList[i];
                    $('#' + text.id + ' .language').val(text['language']);
                }
            }
        );
    },
    fillHtmlDescription: function (description, songId) {
        if (!isNaN(songId)) {
            $.getJSON(songAPI.fetchAllSongSortInfo(),
                function (shortSongsInfoList) {
                    for (let i = 0; i < shortSongsInfoList.length; i++) {
                        if (songId === shortSongsInfoList[i].id){
                            $('#description').text('Texts of Song "' + shortSongsInfoList[i]['songName'] + '" from Album "' + shortSongsInfoList[i]['albumName'] + '"')
                        }
                    }
                }
            );
        }
    },
    loadTexts: function (songId, tableObject, description) {
        let getPath = textAPI.getAllTexts();
        if (!isNaN(songId)) {
            getPath = textAPI.getSongTexts(songId);
        }
        $.getJSON(getPath,
            function (textList) {
                for (let i = 0; i < textList.length; i++) {
                    $(tableObject).append(Texts.composeTextHtmlRow(textList[i]));
                }
                Texts.fillHtmlSongList(textList);
                Texts.fillHtmlLanguageList(textList);
                Texts.fillHtmlDescription(description, songId);

                Texts.onClickSaveTextButton(tableObject, songId);
                Texts.onClickDeleteButton(tableObject);
            }
        );
    },
    onClickSaveTextButton: function (tableObject, songId) {
        $(tableObject).on('click', '.saveButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            const newTextData = {
                "id": $(rowObject).find('.id').text(),
                "songId": $(rowObject).find('.song').val(),
                "language": $(rowObject).find('.language').val(),
                "text": $(rowObject).find('.text').val()
            };
            if (newTextData.songId === '-1') {
                newTextData.songId = '';
            }
            $.patchJSON(textAPI.patch(), newTextData,
                function (result) {
                    $(rowObject).find('.id').text(result.id);
                    $(rowObject).attr("id", result.id);
                    if (!isNaN(songId) && result.songId !== songId) {
                        $('#' + result.id).remove();
                        alert("Saved and moved to other table");
                    } else {
                        alert("Saved");
                    }
                },
                function (result) {
                    alertError(result);
                }
            );
        });
    },
    onClickDeleteButton: function (tableObject) {
        $(tableObject).on('click', '.deleteButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            if (!confirm('Delete text?')) {
                return;
            }
            if ($(rowObject)[0].id[0] === "_") {
                $(rowObject).remove();
            } else {
                $.deleteJSON(textAPI.delete($(rowObject)[0].id),
                    function () {
                        $(rowObject).remove();
                    },
                    function (result) {
                        alertError(result);
                    }
                );
            }
        });
    },
    textCreatingOrder: 0,
    onClickCreateText: function (createNewTextButton, tableObject, songId) {
        $(createNewTextButton).on('click', function () {
            const inCreatingOrder = ++Texts.textCreatingOrder;
            $(tableObject).append(Texts.composeTextHtmlRow({
                id: '_' + inCreatingOrder,
                text: ''
            }));
            $.getJSON(songAPI.fetchAllSongSortInfo(),
                function (shortSongsInfoList) {
                    for (let i = 0; i < shortSongsInfoList.length; i++) {
                        const song = shortSongsInfoList[i];
                        const option = '<option value="' + song.id + '">' + song['albumName'] + ': ' + song['songName'] + '</option>';
                        $('#_' + inCreatingOrder + ' .song').append(option);
                    }
                    if (!isNaN(songId)) {
                        $('#_' + inCreatingOrder + ' .song').val(songId);
                    }
                }
            );
            $.getJSON(languageAPI.getAllLanguages(),
                function (languagesList) {
                    for (let i = 0; i < languagesList.length; i++) {
                        const language = languagesList[i];
                        const option = '<option value="' + language.name + '">' + language.name + '</option>';
                        $('#_' + inCreatingOrder + ' .language').append(option);
                    }
                }
            );
        });
    }
};