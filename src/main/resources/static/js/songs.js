const Songs = {
    composeSongHtmlRow: function (song) {
        return '<tr id="' + song.id + '">' +
            '       <td><p class="id">' + ((song.id[0] === '_') ? "" : song.id) + '</p></td>' +
            '       <td><input class="name" value="' + song.name + '"/></td>' +
            '       <td><select class="album"><option value="-1">No Album</option></select></td>' +
            '       <td><input class="orderInAlbum" value="' + (song['orderInAlbum']) + '" type="number" min="1"/></td>' +
            '       <td><button class="saveButton">Save</button></td>' +
            '       <td><button class="deleteButton">Delete</button></td>' +
            '       <td><button class="textButton">Texts</button></td>' +
            '   </tr>';
    },
    fillHtmlAlbumList: function (songList) {
        $.getJSON(albumAPI.get(),
            function (albumList) {
                for (let i = 0; i < albumList.length; i++) {
                    const album = albumList[i];
                    const option = '<option value="' + album.id + '">' + album.name + ' (id = ' + album.id + ')</option>';
                    $('.album').append(option);
                }
                for (let i = 0; i < songList.length; i++) {
                    const song = songList[i];
                    $('#' + song.id + ' select').val(song['albumId']);
                }
            }
        );
    },
    fillHtmlDescription: function (description, albumId) {
        if (!isNaN(albumId)) {
            $.getJSON(albumAPI.get(),
                function (albumList) {
                    for (let i = 0; i < albumList.length; i++) {
                        if (albumId === albumList[i].id)
                            $('#description').text('Songs of Album "' + albumList[i].name + '"')
                    }
                }
            );
        }
    },
    loadSongs: function (albumId, tableObject, description) {
        $.getJSON(isNaN(albumId) ? songAPI.getAllSongs() : songAPI.getAlbumSongs(albumId),
            function (songList) {
                for (let i = 0; i < songList.length; i++) {
                    $(tableObject).append(Songs.composeSongHtmlRow(songList[i]));
                }
                Songs.fillHtmlAlbumList(songList);
                Songs.fillHtmlDescription(description, albumId);

                Songs.onClickSaveSongButton(tableObject, albumId);
                Songs.onClickDeleteButton(tableObject);
                Songs.onClickTextButton(tableObject);
            }
        );
    },
    onClickSaveSongButton: function (tableObject, albumId) {
        $(tableObject).on('click', '.saveButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            const newSongData = {
                "id": $(rowObject).find('.id').text(),
                "name": $(rowObject).find('.name').val(),
                "albumId": $(rowObject).find('.album').val(),
                "orderInAlbum": $(rowObject).find('.orderInAlbum').val()
            };
            if (newSongData.albumId === '-1') {
                newSongData.albumId = '';
            }
            $.patchJSON(songAPI.patch(), newSongData,
                function (result) {
                    $(rowObject).find('.id').text(result.id);
                    $(rowObject).attr("id", result.id);
                    if (!isNaN(albumId) && result.albumId !== albumId) {
                        $('#' + result.id).remove();
                        alert("Saved and moved to other table");
                    } else {
                        alert("Saved");
                    }
                },
                function (result) {
                    alertError(result);
                }
            );
        });
    },
    onClickDeleteButton: function (tableObject) {
        $(tableObject).on('click', '.deleteButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            if (!confirm('Delete song?')) {
                return;
            }
            if ($(rowObject)[0].id[0] === "_") {
                $(rowObject).remove();
            } else {
                $.deleteJSON(songAPI.delete($(rowObject)[0].id),
                    function () {
                        $(rowObject).remove();
                    },
                    function (result) {
                        alertError(result);
                    }
                );
            }
        });
    },
    onClickTextButton: function (tableObject) {
        $(tableObject).on('click', '.textButton', function () {
            window.location.href = 'texts.html?songId=' + $(this).closest('tr').attr('id');
        });
    },
    songCreatingOrder: 0,
    onClickCreateSong: function (createNewSongButton, tableObject, albumId) {
        $(createNewSongButton).on('click', function () {
            const inCreatingOrder = ++Songs.songCreatingOrder;
            $(tableObject).append(Songs.composeSongHtmlRow({
                id: '_' + inCreatingOrder,
                name: '',
                orderInAlbum: ''
            }));
            $.getJSON(albumAPI.get(),
                function (albumList) {
                    for (let i = 0; i < albumList.length; i++) {
                        const album = albumList[i];
                        const option = '<option value="' + album.id + '">' + album.name + ' (id = ' + album.id + ')</option>';
                        $('#_' + inCreatingOrder + ' .album').append(option);
                    }
                    if (!isNaN(albumId)) {
                        $('#_' + inCreatingOrder + ' .album').val(albumId);
                    }
                }
            );
        });
    }
};