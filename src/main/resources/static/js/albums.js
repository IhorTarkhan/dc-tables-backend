const Albums = {
    composeAlbum: function (album) {
        return '<tr id="' + album.id + '">' +
            '       <td><p class="id">' + ((album.id[0] === '_') ? "" : album.id) + '</p></td>' +
            '       <td><input class="name" value="' + album.name + '"/></td>' +
            '       <td><input class="imageId" value="' + album.imageId + '"/></td>' +
            '       <td><input type="date" placeholder="dd-mm-yyyy" class="releaseDate" value="' + album.releaseDate + '"/></td>' +
            '       <td><button class="saveButton">Save</button></td>' +
            '       <td><button class="deleteButton">Delete</button></td>' +
            '       <td><button class="songsButton">Songs</button></td>' +
            '   </tr>';
    },
    loadAlbums: function (tableObject) {
        $.getJSON(albumAPI.get(),
            function (albumList) {
                for (let i = 0; i < albumList.length; i++) {
                    $(tableObject).append(Albums.composeAlbum(albumList[i]));
                }
                Albums.onClickSaveAlbumButton(tableObject);
                Albums.onClickDeleteAlbumButton(tableObject);
                Albums.onClickSongsButton(tableObject);
            }
        );
    },
    onClickSaveAlbumButton: function (tableObject) {
        $(tableObject).on('click', '.saveButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            const newAlbumData = {
                "id": $(rowObject).find(".id").text(),
                "name": $(rowObject).find(".name").val(),
                "imageId": $(rowObject).find('.imageId').val(),
                "releaseDate": $(rowObject).find('.releaseDate').val()
            };
            $.patchJSON(albumAPI.patch(), newAlbumData,
                function (result) {
                    $(rowObject).find('.id').text(result.id);
                    $(rowObject).attr("id", result.id);
                    alert("Saved");
                },
                function (result) {
                    alertError(result);
                }
            );
        });
    },
    onClickDeleteAlbumButton: function (tableObject) {
        $(tableObject).on('click', '.deleteButton', function () {
            const rowObject = '#' + $(this).closest('tr').attr('id');
            if (!confirm('Delete album?')) {
                return;
            }
            if ($(rowObject)[0].id[0] === "_") {
                $(rowObject).remove();
            } else {
                $.deleteJSON(albumAPI.delete($(rowObject)[0].id),
                    function () {
                        $(rowObject).remove();
                    },
                    function (result) {
                        alertError(result);
                    }
                );
            }
        });
    },
    onClickSongsButton: function (tableObject) {
        $(tableObject).on('click', '.songsButton', function () {
            window.location.href = 'songs.html?albumId=' + $(this).closest('tr').attr('id');
        });
    },
    albumCreatingOrder: 0,
    onClickCreateAlbum: function (createNewAlbumButton, tableObject) {
        $(createNewAlbumButton).on('click', function () {
            const inCreatingOrder = ++Albums.albumCreatingOrder;
            $(tableObject).append(Albums.composeAlbum({
                id: "_" + inCreatingOrder,
                name: "",
                imageId: "",
                releaseDate: ""
            }));
        });
    }
}