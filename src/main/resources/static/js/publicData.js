$(document).ready(function () {
    $.patchJSON = function (url, data, onSuccess, onError) {
        return $.ajax({
            'type': 'PATCH',
            'url': url,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
            "headers": {
                "accept": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            'success': onSuccess,
            'error': onError
        });
    };

    $.deleteJSON = function (url, onSuccess, onError) {
        return $.ajax({
            'type': 'DELETE',
            'url': url,
            'success': onSuccess,
            'error': onError
        });
    };
});

function alertError(result) {
    const message = result.responseJSON.message;
    const errorMessage = message.substring(message.lastIndexOf('Detail: ') + 'Detail: '.length);
    alert('ERROR:\n' + errorMessage);
}

const host = '';

const albumAPI = {
    path: host + '/api/albums',
    get: function () {
        return this.path;
    },
    patch: function () {
        return this.path;
    },
    delete: function (albumId) {
        return this.path + '/' + albumId;
    }
};

const songAPI = {
    path: host + '/api/songs',
    getAllSongs: function () {
        return this.path;
    },
    getAlbumSongs: function (albumId) {
        return this.path + "?albumId=" + albumId;
    },
    patch: function () {
        return this.path;
    },
    delete: function (songId) {
        return this.path + '/' + songId;
    },
    fetchAllSongSortInfo: function () {
        return this.path + "/briefInfo";
    }
};

const textAPI = {
    path: host + '/api/texts',
    getAllTexts: function () {
        return this.path;
    },
    getSongTexts: function (songId) {
        return this.path + "?songId=" + songId;
    },
    patch: function () {
        return this.path;
    },
    delete: function (textId) {
        return this.path + '/' + textId;
    }
};

const languageAPI = {
    path: host + '/api/languages',
    getAllLanguages: function () {
        return this.path;
    },
    patch: function () {
        return this.path;
    },
    delete: function (languageId) {
        return this.path + '/' + languageId;
    }
};