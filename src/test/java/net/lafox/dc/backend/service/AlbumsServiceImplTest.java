package net.lafox.dc.backend.service;

import net.lafox.dc.backend.entity.Album;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AlbumsServiceImplTest {

    @Autowired
    AlbumsService albumsService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getPlayerInfo() {
        List<Album> album = albumsService.findAll();
        assertNotNull(album);
    }
}