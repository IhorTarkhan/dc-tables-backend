-- // create tables
-- Migration SQL that makes the change goes here.

INSERT INTO albums (name, image_id, release_date)
VALUES ('album 1', 'album_1.jpg', '2020-01-01');
INSERT INTO albums (name, image_id, release_date)
VALUES ('album 2', 'album_2.jpg', '2021-01-01');
INSERT INTO albums (name, image_id, release_date)
VALUES ('album 3', 'album_3.jpg', '2022-01-01');

INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 1', (SELECT id FROM albums WHERE name = 'album 1'), 1);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 2', (SELECT id FROM albums WHERE name = 'album 1'), 2);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 3', (SELECT id FROM albums WHERE name = 'album 1'), 3);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 4', (SELECT id FROM albums WHERE name = 'album 2'), 1);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 5', (SELECT id FROM albums WHERE name = 'album 2'), 2);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 6', (SELECT id FROM albums WHERE name = 'album 2'), 3);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 7', (SELECT id FROM albums WHERE name = 'album 2'), 4);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 8', (SELECT id FROM albums WHERE name = 'album 3'), 1);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 9', (SELECT id FROM albums WHERE name = 'album 3'), 2);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 10', (SELECT id FROM albums WHERE name = 'album 3'), 3);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 11', (SELECT id FROM albums WHERE name = 'album 3'), 4);
INSERT INTO songs (name, album_id, order_in_album)
VALUES ('song 12', (SELECT id FROM albums WHERE name = 'album 3'), 5);

INSERT INTO languages (name)
VALUES ('en');
INSERT INTO languages (name)
VALUES ('uk');
INSERT INTO languages (name)
VALUES ('ru');

INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 1'),
        'song 1

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('uk', (SELECT id FROM songs WHERE name = 'song 1'),
        'song 1

some text here
uk uk uk
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('ru', (SELECT id FROM songs WHERE name = 'song 2'),
        'song 2

some text here
ru ru ru
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 2'),
        'song 2

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('uk', (SELECT id FROM songs WHERE name = 'song 3'),
        'song 3

some text here
uk uk uk
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 3'),
        'song 3

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('ru', (SELECT id FROM songs WHERE name = 'song 3'),
        'song 3

some text here
ru ru ru
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 6'),
        'song 6

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('uk', (SELECT id FROM songs WHERE name = 'song 7'),
        'song 7

some text here
uk uk uk
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 9'),
        'song 9

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('ru', (SELECT id FROM songs WHERE name = 'song 10'),
        'song 10

some text here
ru ru ru
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('ru', (SELECT id FROM songs WHERE name = 'song 11'),
        'song 11

some text here
ru ru ru
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('en', (SELECT id FROM songs WHERE name = 'song 11'),
        'song 11

some text here
en en en
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('ru', (SELECT id FROM songs WHERE name = 'song 12'),
        'song 12

some text here
ru ru ru
some text here');
INSERT INTO texts (language, song_id, text)
VALUES ('uk', (SELECT id FROM songs WHERE name = 'song 12'),
        'song 12

some text here
uk uk uk
some text here');

-- //@UNDO
-- SQL to undo the change goes here.

DELETE
FROM texts
WHERE TRUE;
DELETE
FROM languages
WHERE TRUE;
DELETE
FROM songs
WHERE TRUE;
DELETE
FROM albums
WHERE TRUE;