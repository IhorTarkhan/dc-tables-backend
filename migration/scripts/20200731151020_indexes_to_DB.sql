-- // indexes to DB
-- Migration SQL that makes the change goes here.

CREATE INDEX album_id ON songs (album_id);
CREATE INDEX language_of_text ON texts (language);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX IF EXISTS album_id;
DROP INDEX IF EXISTS language_of_text;
