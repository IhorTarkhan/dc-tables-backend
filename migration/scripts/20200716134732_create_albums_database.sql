-- // create tables
-- Migration SQL that makes the change goes here.

CREATE TABLE albums
(
    id           BIGSERIAL PRIMARY KEY,
    name         TEXT NOT NULL UNIQUE,
    image_id     TEXT,
    release_date DATE NOT NULL UNIQUE
);

CREATE TABLE songs
(
    id         BIGSERIAL PRIMARY KEY,
    name       TEXT      NOT NULL,
    album_id   BIGSERIAL NOT NULL REFERENCES albums (id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    order_in_album int       NOT NULL,
    UNIQUE (album_id, order_in_album)
);

CREATE TABLE languages
(
    name TEXT PRIMARY KEY
);

CREATE TABLE texts
(
    id         BIGSERIAL PRIMARY KEY,
    text text,
    language   text NOT NULL references languages (name)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    song_id    BIGSERIAL REFERENCES songs (id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    UNIQUE (language, song_id)
);

-- //@UNDO
-- SQL to undo the change goes here.

DROP TABLE texts;
DROP TABLE languages;
DROP TABLE songs;
DROP TABLE albums;