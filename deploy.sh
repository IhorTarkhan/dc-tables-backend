#!/usr/bin/env bash

HOST=ihor@ihor.lafox.net;

#for file in src/main/resources/static/css/*.less; do lessc -x --strict-imports $file "${file%.less}.css" ; done

#mvn clean install -Dmaven.test.skip=true

#rsync -r -v --progress -e "ssh -p 22" target/darwins-cat-*.jar $HOST:~/

#read -r -p "Restart Web server? [Y/n]" response
#if [[ $response =~ ^(yes|y|Y| ) ]] || [[ -z $response ]]; then
#    ssh $HOST /home/lafox/restart.sh
#fi


rsync -r -v --progress -e "ssh -p 22" migration/ $HOST:~/migration/

ssh $HOST migration/status.sh

read -r -p "Start DB migrattion? [Y/n]" response
if [[ $response =~ ^(yes|y|Y| ) ]] || [[ -z $response ]]; then
    ssh $HOST migration/migrate.sh
fi